function randomPalette(){

  var R = (Math.floor(Math.random() * 256) - 1);
  var G = (Math.floor(Math.random() * 256) - 1);
  var B = (Math.floor(Math.random() * 256) - 1);

  process (R, G, B, '1');

  document.querySelector('body').style['background-color']=document.getElementById('color1').style['background-color'];
  document.querySelector('body').style['color']=document.getElementById('color2').style['background-color'];
  document.querySelector('body').style['border-color']=document.getElementById('color3').style['background-color']
  document.querySelector('textarea').style['border-color']=document.getElementById('color3').style['background-color']
  var i;
  for(i=1;i<6;i++){
  document.getElementById('color'+i).style['border-color']=document.getElementById('color3').style['background-color']
  }
}

function process (R, G, B, n){

  if(n<6){

    var hsl = rgbToHsl(R,G,B);

    hsl[0] = hsl[0] + 1/5;
    if(hsl[0]>1){
      hsl[0]=hsl[0]-1;
    }

    var rgb = hslToRgb(hsl[0], hsl[1], hsl[2]);

    var R = Math.floor(rgb[0]);
    var G = Math.floor(rgb[1]);
    var B = Math.floor(rgb[2]);

    var style = 'background-color: rgb('+R+','+G+','+B+')';

    document.getElementById('color'+n).style = style;
    return process(R, G, B, ++n);
  }

  return;

}

function cleanPalette(){

  var style = 'background-color: rgb('+255+','+255+','+255+')';
  document.getElementById('color1').style = style;
  document.getElementById('color2').style = style;
  document.getElementById('color3').style = style;
  document.getElementById('color4').style = style;
  document.getElementById('color5').style = style;

  document.querySelector('body').style['background-color']=document.getElementById('color1').style['background-color'];
  document.querySelector('body').style['color']='rgb('+0+','+0+','+0+')';

}
