Cypress

En cypress cuando se invoca al inicio y al final el comando cy.screenshot()
Este solo es ejecutado al final

ResembleJs

Con resembleJs se puede obtener un analisis basico de una imagen en donde se muestra el brillo y sus colores en rgb.

Al comparar Resemble valida lo siguiente:
misMatchPercentage : Porcentaje de falta de coincidencia
isSameDimensions: Verifica si es de la misma dimensión
dimensionDifference: { width: 0, height: -1 }, muestra un resultado si las dimenciones no son las mismas

De las opciones que se pueden seleccionar estan:

Es posible reducir el área de comparación, especificando un cuadro delimitador medido en píxeles desde la esquina superior izquierda: resemble.outputSettings
largeImageThreshold: Esta variable se debe tener en cuenta para la omision de pixeles, el algoritme omite cuando las imagenes son mas grandes que 1200 pixeles.
useCrossOrigin: Es True por defecto, pero se puede dejar en falso si se esta utilizando una URI de datos.

