var http = require('http')
var express = require('express')
var path = require('path')

var app = express();

var port = process.env.PORT || 8889

app.use(express.static(path.join(__dirname, '/public/')))

var server = http.createServer(app).listen(port, function(){
console.log('Express server listening on port ' + port);
});


